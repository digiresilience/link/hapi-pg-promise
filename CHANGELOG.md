# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.0.3](https://digiresilience.org/link/hapi-pg-promise/compare/0.0.2...0.0.3) (2021-10-08)

### [0.0.2](https://digiresilience.org/link/hapi-pg-promise/compare/0.0.1...0.0.2) (2021-05-03)


### Features

* update deps ([3d8609a](https://digiresilience.org/link/hapi-pg-promise/commit/3d8609ae069d6c43da8a2a7d3a6b7c7e0e7db014))

### 0.0.1 (2020-11-20)

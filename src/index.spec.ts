import * as Hapi from "@hapi/hapi";
import PgPromisePlugin from ".";

describe("plugin option validation", () => {
  let server;
  beforeEach(async () => {
    server = new Hapi.Server();
  });

  it("should throw when no connection details defined", async () => {
    expect(server.register(PgPromisePlugin)).rejects.toThrow();
  });
});

const defaultOpts = {
  connection: "postgresql://amigo:amigo@127.0.0.1:5432/postgres",
};
describe("basic plugin runtime", () => {
  let server;
  beforeEach(async () => {
    server = new Hapi.Server({ port: 0 });
    await server.register({
      plugin: PgPromisePlugin,
      options: defaultOpts,
    });
    await server.start();
  });
  afterEach(async () => {
    await server.stop();
  });

  it("should decorate db and pgp into server and request", async () => {
    expect.assertions(5);
    server.route({
      method: "GET",
      path: "/",
      handler: (req) => {
        expect(req.db).toBeInstanceOf(Function);
        expect(req.server.db).toBeInstanceOf(Function);
        expect(req.pgp).toBeInstanceOf(Function);
        expect(req.server.pgp).toBeInstanceOf(Function);
        return "OK";
      },
    });

    const { statusCode } = await server.inject({
      method: "get",
      url: "/",
    });
    expect(statusCode).toBe(200);
  });
});

describe("plugin runtime", () => {
  let server;
  beforeEach(async () => {
    server = new Hapi.Server({ port: 0 });
  });
  afterEach(async () => {
    await server.stop();
  });
  it("should decorate db and pgp into server and request with custom name", async () => {
    expect.assertions(5);

    await server.register({
      plugin: PgPromisePlugin,
      options: {
        ...defaultOpts,
        logSql: true,
        decorateAs: {
          pgp: "foobar",
          db: "poprocks",
        },
      },
    });

    await server.start();

    await server.route({
      method: "GET",
      path: "/",
      handler: (req) => {
        expect(req.poprocks).toBeInstanceOf(Function);
        expect(req.server.poprocks).toBeInstanceOf(Function);
        expect(req.foobar).toBeInstanceOf(Function);
        expect(req.server.foobar).toBeInstanceOf(Function);
        return "OK";
      },
    });

    const { statusCode } = await server.inject({
      method: "get",
      url: "/",
    });
    expect(statusCode).toBe(200);
  });

  it("should not decorate db and pgp into server and request", async () => {
    expect.assertions(5);

    await server.register({
      plugin: PgPromisePlugin,
      options: {
        ...defaultOpts,
        logSql: true,
        decorateAs: {
          pgp: false,
        },
      },
    });

    await server.start();

    await server.route({
      method: "GET",
      path: "/",
      handler: (req) => {
        expect(req.pgp).toBeUndefined();
        expect(req.server.pgp).toBeUndefined();
        expect(req.db).toBeInstanceOf(Function);
        expect(req.server.db).toBeInstanceOf(Function);
        return "OK";
      },
    });

    const { statusCode } = await server.inject({
      method: "get",
      url: "/",
    });
    expect(statusCode).toBe(200);
  });
});

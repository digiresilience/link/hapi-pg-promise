import type { IMain, IInitOptions, IDatabase } from "pg-promise";
import type { IConnectionParameters } from "pg-promise/typescript/pg-subset";

export type ExtendedProtocol<T> = IDatabase<T> & T;

export type IPGPPluginOptions<T> = {
  connection: string | IConnectionParameters;
  logSql?: boolean;
  decorateAs?: {
    pgp: string | boolean;
    db: string | boolean;
  };
} & ({ pgpInit: IInitOptions<T> } | { pgp: IMain<T> });

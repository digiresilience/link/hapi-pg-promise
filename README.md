# hapi-pg-promise

This is a plugin for hapi.js that decorates server and request with a [pg-promise](https://github.com/vitaly-t/pg-promise) database instance.

## Usage

```typescript
import * as Hapi from "@hapi/hapi";
import PgPromisePlugin from "@digiresilience/hapi-pg-promise";

const server = new Hapi.Server();

// the validator must be registered before registering the plugin
await server.validator(Joi);

// register the plugin
await server.register({
  plugin: PgPromisePlugin,
  options: {
    // the only required parameter is the connection string
    connection: "postgresql://....",
    // ... and the pg-promise initialization options
    pgpInit: {...}
  },
});
```

Reference the [pg-promise initialization options](http://vitaly-t.github.io/pg-promise/module-pg-promise.html)

Options consist of:

- `connection` - pg-promise/pg connection string or object
- `pgpInit` - the pg-promise initialization options (mutually exclusive with `pgp`)
- `pgp` - an existing pre-initialized pg-promise instance (mutually exclusive with `pgpInit`)
- `logSql` - a boolean that when true, causes pgp monitor to print all sql statements. !! WARNING !! setting to true could cause data leaks
- `decorateAs` - an object containing..
  - `pgp` - a string. the plugin will decorate server and request with the pgp instance under this key (default: pgp)
  - `db` - a string. the plugin will decorate server and request with the db instance under this key (default: db)
  - if either of these is falsey, then the plugin will not decorate

Defaults are defined in [`src/index.ts`](src/index.ts)

## Credits

Copyright © 2020-present [Center for Digital Resilience][cdr]

### Contributors

| [![Abel Luck][abelxluck_avatar]][abelxluck_homepage]<br/>[Abel Luck][abelxluck_homepage] |
| ---------------------------------------------------------------------------------------- |


[abelxluck_homepage]: https://gitlab.com/abelxluck
[abelxluck_avatar]: https://secure.gravatar.com/avatar/0f605397e0ead93a68e1be26dc26481a?s=100&d=identicon

### License

[![License: AGPL v3](https://img.shields.io/badge/License-AGPL%20v3-blue.svg)](https://www.gnu.org/licenses/agpl-3.0.en.html)

    GNU AFFERO GENERAL PUBLIC LICENSE
    Version 3, 19 November 2007

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

[cdrtech]: https://digiresilience.org/tech/
[cdr]: https://digiresilience.org
